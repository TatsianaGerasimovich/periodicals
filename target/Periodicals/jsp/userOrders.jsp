<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 31.05.2015
  Time: 22:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="locale"
       value="${locale}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="messages"/>
<html class="no-js">
<head>
<meta charset="utf-8">
<title>Orders</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">

<link rel="stylesheet" href="jsp/css/bootstrap.min.css">
<link rel="stylesheet" href="jsp/css/normalize.min.css">
<link rel="stylesheet" href="jsp/css/font-awesome.min.css">
<link rel="stylesheet" href="jsp/css/animate.css">
<link rel="stylesheet" href="jsp/css/templatemo_misc.css">
<link rel="stylesheet" href="jsp/css/templatemo_style.css">
<script src="jsp/js/vendor/jquery-1.10.1.min.js"></script>
<script>window.jQuery || document.write('<script src="jsp/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="jsp/js/jquery.easing-1.3.js"></script>
<script src="jsp/js/vendor/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript">
        function cancelChanging() {

            $('#orderList').html(tableState);

            //removing delete buttons
            $('.btn-danger ').remove();

            //removing save button
            $('#SaveOrderBtn').remove();
            $('#CancelBtn').remove();

            //making buttons disabel
            if (!(($('#orderList tr').size()) == 1)) {
                $('#changeOrderBtn').show();
                document.getElementById('changeOrderBtn').removeAttribute('disabled');
            }
            else{
                $('#changeOrderBtn').show();
                document.getElementById('changeOrderBtn').setAttribute('disabled', 'disabled');
            }
        }

        var tableState;

        function removeTr(id) {
            var del = $('#' + id);
            del.remove();
            if ((($('#orderList tr').size()) == 1)) {
                document.getElementById('makeOrderBtn').setAttribute('disabled', 'disabled');
            }
        }

        function saveOrder() {

            var rows = $('#orderList tbody tr');
            var orderMass = [];
            var id;
            for (var i = 0; i < rows.length; i++) {
                id = $(rows[i]).attr('id');
                orderMass.push({id: id});
            }

            var request = new Object();
            request.data = JSON.stringify(orderMass);
            request.command = 'update_orders';

            $.ajax({
                url: "Controller",
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(request),
                contentType: 'application/json',
                mimeType: 'application/json',

                success: function (data) {
                    if (data.id == -1) {
                        location.reload();
                    }
                    else {
                        $('#errorMess').attr('style', 'visibility: visible;')
                    }
                },
                error: function (data, status, er) {
                    $('#errorMess').attr('style', 'visibility: visible;')
                    alert("error: " + data + " status: " + status + " er:" + er);
                }
            });
        }

        function changeOrders() {

            tableState = $('#orderList').html();
            //adding delete buttons

            var rows = $('#orderList tbody tr');
            for (var i = 0; i < rows.length; i++) {
                var last = $(rows[i]).find('td:last');
                var trId = $(last).closest('tr').attr('id');
                var deleteBtn = '<button class="btn btn-danger " type="button" onclick="removeTr(' + trId + ')"> <fmt:message key="orders.btn.remove"/></button>';
                $(last).after(deleteBtn);
            }
            //adding save button
            var btn = '<br><button class="btn btn-success " type="button" id="SaveOrderBtn" onclick="saveOrder()"> <fmt:message key="cart.saveChanges"/></button>';
            var btn2 = '<button class="btn btn-danger " type="button" id="CancelBtn" onclick="cancelChanging()"> <fmt:message key="cart.cancel"/></button>';
            $('#changeOrderBtn').after(btn);
            $('#SaveOrderBtn').after(btn2);

            document.getElementById('changeOrderBtn').setAttribute('disabled', 'disabled');
            $('#changeOrderBtn').hide();
        }

    </script>
</head>
<body>
<%@include file="header.jsp" %>

<div id="log" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="section-title"> <fmt:message key="user.orders.title"/></h1>
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-md-2 col-sm-3">
                <c:forEach var="photo_f" items="${first_photos}">
                    <div class="item-small">
                        <img src="${photo_f.coverOfPeriodical}" alt="Product 1">
                    </div>
                    <!-- /.item-small -->
                    <br/>
                </c:forEach>
            </div>
            <!-- /.col-md-2 -->

            <div class="col-md-8 col-sm-6">

                <div class="item-large">

                    <form method="post" action="Controller">
                        <table class="table " id="orderList">
                            <thead>
                            <th>№</th>
                            <th><fmt:message key="orders.periodical"/></th>
                            <th><fmt:message key="orders.number"/></th>
                            <th><fmt:message key="orders.date"/></th>
                            <th><fmt:message key="orders.payment_status"/></th>
                            </thead>
                            <c:forEach items="${orders}" var="order" varStatus="status">
                                <tr id="${order.id}">
                                    <td width="40px" >${status.index+1}</td>
                                    <td width="600px">
                                        <ul>
                                            <c:forEach items="${order.periodicals}" var="periodicalInf">
                                                <li>${periodicalInf.periodical.periodicalName}</li>
                                            </c:forEach>
                                        </ul>
                                    </td>
                                    <td width="20px">
                                        <ul>
                                            <c:forEach items="${order.periodicals}" var="periodicalInf">
                                                <li>${periodicalInf.number}</li>
                                            </c:forEach>
                                        </ul>
                                    </td>
                                    <td width="300px"><fmt:formatDate value="${order.dateOfOrder}"
                                                                      pattern="dd-MM-yyyy"/></td>
                                    <td width="100px">
                                        <c:if test="${order.status == 0}"><fmt:message key="user.orders.not_paid"/></c:if>
                                        <c:if test="${order.status == 1}"><fmt:message key="user.orders.paid"/></c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                        <br>
                        <button class="btn btn-default " type="button" id="changeOrderBtn"
                                <c:if test="${orders.size() == 0}">disabled="disabled" </c:if> onclick="changeOrders()">
                            <fmt:message key="comments.btn.change"/>
                        </button>
                        <br>

                        <p id="errorMes" style="visibility: hidden;"><fmt:message key="order.errorMess"/></p>
                    </form>
                </div>
                <!-- /.item-large -->

            </div>
            <!-- /.col-md-8 -->

            <div class="col-md-2 col-sm-3">
                <c:forEach var="photo" items="${last_photos}">
                    <div class="item-small">
                        <img src="${photo.coverOfPeriodical}" alt="Product 1">
                    </div>
                    <!-- /.item-small -->
                    <br/>
                </c:forEach>
            </div>
            <!-- /.col-md-2 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /#product-promotion -->


<div class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                    <span>
                    	Copyright &copy; 2015 Gerasimovich Tatiana
                    </span>

            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6">
                <ul class="social">
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-instagram"></a></li>
                    <li><a href="#" class="fa fa-linkedin"></a></li>
                    <li><a href="#" class="fa fa-rss"></a></li>
                </ul>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /.site-footer -->


<!-- Scroll to Top -->
<div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>

<script src="jsp/js/bootstrap.js"></script>
<script src="jsp/js/plugins.js"></script>
<script src="jsp/js/main.js"></script>
<script type="jsp/text/javascript">

    $(document).ready(function() {
        $(function() {
            $(window).scroll(function() {
                if($(this).scrollTop() != 0) {
                    $('#toTop').fadeIn();
                } else {
                    $('#toTop').fadeOut();
                }
            });

            $('#toTop').click(function() {
                $('body,html').animate({scrollTop:0},800);
            });
        });
    });

</script>
</body>
</html>