<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 31.05.2015
  Time: 20:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="locale"
       value="${locale}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="messages"/>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <title>Cart</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="jsp/css/bootstrap.min.css">
    <link rel="stylesheet" href="jsp/css/normalize.min.css">
    <link rel="stylesheet" href="jsp/css/font-awesome.min.css">
    <link rel="stylesheet" href="jsp/css/animate.css">
    <link rel="stylesheet" href="jsp/css/templatemo_misc.css">
    <link rel="stylesheet" href="jsp/css/templatemo_style.css">
    <script src="jsp/js/vendor/jquery-1.10.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="jsp/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
    <script src="jsp/js/jquery.easing-1.3.js"></script>

    <script src="jsp/js/vendor/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript">
    jQuery.fn.ForceNumericOnly =
            function () {
                return this.each(function () {
                    $(this).keydown(function (e) {
                        var key = e.charCode || e.keyCode || 0;
                        // Разрешаем backspace, tab, delete, стрелки, обычные цифры и цифры на дополнительной клавиатуре
                        return (
                                key == 8 ||
                                key == 9 ||
                                key == 46 ||
                                (key >= 37 && key <= 40) ||
                                (key >= 48 && key <= 57) ||
                                (key >= 96 && key <= 105));
                    });
                });
            };

    function checkNumber(k) {
        var str = jQuery(k).val();
        if ( str <= 0 || !str) {
            $(k).closest('.form-group').addClass('has-error');
            if ($('#SaveOrderBtn').attr('disabled') == undefined) {
                $(k).closest('tr').find('.btn').after('<b class="text-danger"> <fmt:message key="cart.invalid"/></b>');
                document.getElementById('SaveOrderBtn').setAttribute('disabled', 'disabled');
            }
        }
        else {
            $(k).closest('.form-group').removeClass('has-error');
            $(k).closest('tr').find('.text-danger').remove();
            document.getElementById('SaveOrderBtn').removeAttribute('disabled');
        }
    }

    function cancelChanging() {

        $('#orderList').html(tableState);

        //removing delete buttons
        $('.btn-danger ').remove();

        //removing save button
        $('#SaveOrderBtn').remove();
        $('#CancelBtn').remove();

        //making buttons disabel
        if (!(($('#orderList tr').size()) == 1)) {
            $('#makeOrderBtn').show();
            $('#changeOrderBtn').show();
            document.getElementById('makeOrderBtn').removeAttribute('disabled');
            document.getElementById('changeOrderBtn').removeAttribute('disabled');
        }
        else{
            $('#makeOrderBtn').show();
            $('#changeOrderBtn').show();
             document.getElementById('changeOrderBtn').setAttribute('disabled', 'disabled');
             document.getElementById('makeOrderBtn').setAttribute('disabled', 'disabled');
        }
    }

    var tableState;

    function removeTr(id) {
        var del = $('#' + id);
        del.remove();
        if ((($('#orderList tr').size()) == 1)) {
            document.getElementById('makeOrderBtn').setAttribute('disabled', 'disabled');
        }
    }

    function saveOrder() {

        $('.form-control').each(function () {
            checkNumber(this)
        });

        var rows = $('#orderList tbody tr');
        var cart = [];
        var id;
        var periodical;
        var number;
        for (var i = 0; i < rows.length; i++) {
            id = $(rows[i]).attr('id');
            periodical = {id: id}
            if($(rows[i]).find('input').val()<65534) {
                number = $(rows[i]).find('input').val();
            }
            else {
                alert("Not valid format");
                number =1;

            }
            cart.push({periodical: periodical, number: number});
        }

        var request = new Object();
        request.data = JSON.stringify(cart);
        request.command = 'update_cart';

        $.ajax({
            url: "Controller",
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(request),
            contentType: 'application/json',
            mimeType: 'application/json',

            success: function (data) {
                if (data == -1) {
                    location.reload();
                }
                else {
                    $('#' + data.status).find('td:last').text(data.number);
                    checkNumber($('#' + data.status).find('input'), data.number);
                }
            },
            error: function (data, status, er) {
                alert("error: " + data + " status: " + status + " er:" + er);
            }
        });
    }

    function changeCart() {

        tableState = $('#orderList').html();
        //adding delete buttons

        var rows = $('#orderList tbody tr');
        for (var i = 0; i < rows.length; i++) {
            var last = $(rows[i]).find('td:last');
            var trId = $(last).closest('tr').attr('id');
            var deleteBtn = '<button class="btn btn-danger " type="button" onclick="removeTr(' + trId + ')"><fmt:message key="cart.remove"/></button>';
            $(last).after(deleteBtn);
        }

        //adding save button
        var btn = '<br><button class="btn btn-success " type="button" id="SaveOrderBtn" onclick="saveOrder()"><fmt:message key="cart.save"/></button>';
        var btn2 = '<button class="btn btn-danger " type="button" id="CancelBtn" onclick="cancelChanging()"><fmt:message key="cart.cancel"/></button>';
        $('#changeOrderBtn').after(btn);
        $('#SaveOrderBtn').after(btn2);
        $('#changeOrderBtn').hide();
        $('#makeOrderBtn').hide();
       // document.getElementById('changeOrderBtn').setAttribute('disabled', 'disabled');
      //  document.getElementById('makeOrderBtn').setAttribute('disabled', 'disabled');

        //adding textarea on numbers
        var numbers = $('.periodical-number');
        for (var i = 0; i < numbers.length; i++) {
            var number = $(numbers[i]).text();
            var input = '<div class="form-group"><input name="1" type="text"  class="form-control" value="' + number + '"><div/>';
            $(numbers[i]).html(input)
        }

        //adding controll of textareas
        $('.form-control').each(function () {
            $(this).ForceNumericOnly();
            $(this).keyup(function (event) {
                checkNumber(this);
            })

        })
    }

   function createOrder() {
        var rows = $('#orderList tbody tr');
        var periodicalInf = [];

        var id;
        var number;
        for (var i = 0; i < rows.length; i++) {
            id = $(rows[i]).attr('id');
            number = $(rows[i]).find('.periodical-number').text();
            var periodical = {id: id};
            periodicalInf.push({periodical: periodical, number: number});
        }
        var data = {periodicals: periodicalInf};

        var request = new Object();
        request.data = JSON.stringify(data);
        request.command = 'create_order';

        $.ajax({
            url: "Controller",
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(request),
            contentType: 'application/json',
            mimeType: 'application/json',

            success: function (data) {
                if (data.code == -1) {
                    $("input[name='command']").after('<p class="text-danger"> data.message<p/>')
                }
                else {
                    window.location.replace('Controller?command=ORDER_LIST')
                }
            },
            error: function (data, status, er) {
                alert("error: " + data + " status: " + status + " er:" + er);
            }
        });
    }
    </script>

</head>
<body>
<%@include file="header.jsp" %>

<div id="log" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="section-title"><fmt:message key="cart.title"/></h1>
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-md-2 col-sm-3">
                <c:forEach var="photo_f" items="${first_photos}">
                    <div class="item-small">
                        <img src="${photo_f.coverOfPeriodical}" alt="Product 1">
                    </div>
                    <!-- /.item-small -->
                    <br/>
                </c:forEach>
            </div>
            <!-- /.col-md-2 -->

            <div class="col-md-8 col-sm-6">

                <div class="item-large">

                    <form method="post" action="Controller">
                        <input type="hidden" name="command" value="make_order">
                        <table class="table " id="orderList">
                            <thead>
                            <th><fmt:message key="orders.periodical"/></th>
                            <th><fmt:message key="cart.name"/></th>
                            <th><fmt:message key="orders.number"/></th>
                            </thead>
                            <c:forEach items="${resultMap}" var="periodicalInf">
                                <tr id="${periodicalInf.key.periodicalId}">
                                    <td width="50px"><img src="${periodicalInf.key.coverOfPeriodical}" class="inOrder"></td>
                                    <td width="200px" >${periodicalInf.key.periodicalName}</td>
                                    <td width="200px" class="periodical-number">${periodicalInf.value}</td>
                                </tr>
                            </c:forEach>
                        </table>
                        <br>
                        <button class="btn btn-success " type="button" id="makeOrderBtn" onclick="createOrder()"
                                <c:if test="${user.cart.size == 0}">disabled="disabled" </c:if>  ><fmt:message key="orders.create"/></button>
                        <button class="btn btn-default " type="button" id="changeOrderBtn"
                                <c:if test="${user.cart.size == 0}">disabled="disabled" </c:if> onclick="changeCart()">
                            <fmt:message key="cart.change"/></button>
                    </form>
                </div>
                <!-- /.item-large -->

            </div>
            <!-- /.col-md-8 -->

            <div class="col-md-2 col-sm-3">
                <c:forEach var="photo" items="${last_photos}">
                    <div class="item-small">
                        <img src="${photo.coverOfPeriodical}" alt="Product 1">
                    </div>
                    <!-- /.item-small -->
                    <br/>
                </c:forEach>
            </div>
            <!-- /.col-md-2 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /#product-promotion -->


<div class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                    <span>
                    	Copyright &copy; 2015 Gerasimovich Tatiana
                    </span>

            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6">
                <ul class="social">
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-instagram"></a></li>
                    <li><a href="#" class="fa fa-linkedin"></a></li>
                    <li><a href="#" class="fa fa-rss"></a></li>
                </ul>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /.site-footer -->


<!-- Scroll to Top -->
<div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>


<script src="jsp/js/bootstrap.js"></script>
<script src="jsp/js/plugins.js"></script>
<script src="jsp/js/main.js"></script>
<script type="jsp/text/javascript">

    $(document).ready(function() {
        $(function() {
            $(window).scroll(function() {
                if($(this).scrollTop() != 0) {
                    $('#toTop').fadeIn();
                } else {
                    $('#toTop').fadeOut();
                }
            });

            $('#toTop').click(function() {
                $('body,html').animate({scrollTop:0},800);
            });
        });
    });

</script>
</body>
</html>
