package by.bsuir.gerasimovich.periodicals.controller.command.impl.user;

import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.entity.Users;
import by.bsuir.gerasimovich.periodicals.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * logging out
 *
 * @author Tatiana
 * @version 1.00 28.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class LogOutCommand implements ICommand {
    /**
     * Constants to set request parameters
     */
    private static final String USER = "user";
    /**
     * Override method that call special logic of
     * logging out
     *
     * @param request
     * @param response
     * @return String(forward page)
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = PageHelper.getInstance().getProperty(PageHelper.MAIN_PAGE);
        HttpSession session = request.getSession();
        Users newUser = UserService.getInstance().logOut();
        session.setAttribute(USER, newUser);
        return page;
    }
}
