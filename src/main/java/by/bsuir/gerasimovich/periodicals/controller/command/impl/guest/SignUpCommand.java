package by.bsuir.gerasimovich.periodicals.controller.command.impl.guest;

import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.entity.Users;
import by.bsuir.gerasimovich.periodicals.service.GuestService;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * logging up
 *
 * @author Tatiana
 * @version 1.00 28.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class SignUpCommand implements ICommand {
    /**
     * Constants to get request parameters
     */
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String NAME = "name";
    private static final String EMAIL = "email";
    private static final String COUNTRY = "country";
    private static final String CITY = "city";
    private static final String ADDRESS = "address";
    private static final String POSTCODE = "postcode";
    private static final String PHOTO = "photo";

    /**
     * Constants to set request parameters
     */
    private static final String USER = "user";
    private static final String ERROR_MESSAGE = "errorLoginPassMessage";

    /**
     * Override method that call special logic of
     * logging up
     *
     * @param request
     * @param response
     * @return String(forward page)
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String page = null;
        PageHelper pageHelper = PageHelper.getInstance();

        String login = (String) request.getAttribute(LOGIN);
        String password = (String) request.getAttribute(PASSWORD);
        String name = (String) request.getAttribute(NAME);
        String email = (String) request.getAttribute(EMAIL);
        String country = (String) request.getAttribute(COUNTRY);
        String city = (String) request.getAttribute(CITY);
        String address = (String) request.getAttribute(ADDRESS);
        String photo = (String) request.getAttribute(PHOTO);
        double postcode = Double.parseDouble((String) request.getAttribute(POSTCODE));
        HttpSession session = request.getSession();

        try {
            Users user = GuestService.getInstance().signUp(login, password, name, email, country, city, address, postcode, photo);

            session.setAttribute(USER, user);
            if (user == null) {
                page = pageHelper.getProperty(PageHelper.SIGN_UP_PAGE);
                request.setAttribute(ERROR_MESSAGE, "Неправильный логин или пароль, попробуйте еще раз.");
            } else {
                session.setAttribute(USER, user);
                page = pageHelper.getProperty(PageHelper.PROFILE);
            }

        } catch (NumberFormatException ex) {
            throw new CommandException(ex);
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }

        return page;
    }
}