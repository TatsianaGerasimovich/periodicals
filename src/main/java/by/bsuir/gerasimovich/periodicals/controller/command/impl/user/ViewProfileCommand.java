package by.bsuir.gerasimovich.periodicals.controller.command.impl.user;

import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * transition to the page of profile
 *
 * @author Tatiana
 * @version 1.00 30.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class ViewProfileCommand implements ICommand {

    /**
     * Override method that execute
     * transition to the page of profile
     *
     * @param request
     * @param response
     * @return String(forward page)
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        return PageHelper.getInstance().getProperty(PageHelper.PROFILE);

    }
}
