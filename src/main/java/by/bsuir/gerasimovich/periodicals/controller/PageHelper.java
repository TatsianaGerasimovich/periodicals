package by.bsuir.gerasimovich.periodicals.controller;

import java.util.ResourceBundle;

/**
 * @author Tatiana
 * @version 1.00 18.05.2015.
 */
public class PageHelper {

    public static final String MAIN_PAGE = "MAIN_PAGE";
    public static final String ERROR_PAGE = "ERROR_PAGE";
    public static final String FILTER_ERROR_PAGE = "FILTER_ERROR_PAGE";
    public static final String PERIODICALS_LIST_PAGE = "PERIODICALS_LIST_PAGE";
    public static final String PERIODICAL_PAGE = "PERIODICAL_PAGE";
    public static final String SIGN_IN_PAGE = "SIGN_IN_PAGE";
    public static final String SIGN_UP_PAGE = "SIGN_UP_PAGE";
    public static final String CART_PAGE = "CART_PAGE";
    public static final String ORDERS_PAGE = "ORDERS_PAGE";
    public static final String COMMENTS_PAGE = "COMMENTS_PAGE";
    public static final String PROFILE = "PROFILE";
    public static final String USERS_PAGE = "USERS_PAGE";
    public static final String ALL_ORDERS_PAGE = "ALL_ORDERS_PAGE";
    public static final String PERIODICALS_LIST_FOR_DELETE_PAGE = "PERIODICALS_LIST_FOR_DELETE_PAGE";
    public static final String NEW_PERIODICAL_PAGE = "NEW_PERIODICAL_PAGE";
    private static final String BUNDLE_NAME = "pages";
    private static PageHelper instance;
    private ResourceBundle resourceBundle;


    private PageHelper() {
        resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
    }

    public static PageHelper getInstance() {
        PageHelper localInstance = instance;
        if (localInstance == null) {
            synchronized (PageHelper.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new PageHelper();
                }
            }
        }
        return localInstance;
    }

    public String getProperty(String key) {
        return (String) resourceBundle.getObject(key);
    }
}
