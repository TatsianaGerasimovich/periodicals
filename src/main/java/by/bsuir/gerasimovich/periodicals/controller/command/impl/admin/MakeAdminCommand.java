package by.bsuir.gerasimovich.periodicals.controller.command.impl.admin;

import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.entity.Users;
import by.bsuir.gerasimovich.periodicals.service.AdminService;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * the transfer of the particular user to administrator
 *
 * @author Tatiana
 * @version 1.00 01.06.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class MakeAdminCommand implements ICommand {
    /**
     * Constants to get request parameters
     */
    private static final String JSON = "json";

    /**
     * Override method that call special logic of
     * transfer of the particular user to administrator
     *
     * @param request
     * @param response
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        try {
            Users user = mapper.readValue(json, Users.class);
            AdminService.getInstance().makeUserAdmin(user);
            mapper.writeValue(response.getOutputStream(), -1);
        } catch (IOException | ServiceException ex) {
            throw new CommandException(ex);
        }
        return null;
    }
}

