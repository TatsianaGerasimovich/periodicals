package by.bsuir.gerasimovich.periodicals.dao.mysql;

import by.bsuir.gerasimovich.periodicals.dao.AbstractJDBCDao;
import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;
import by.bsuir.gerasimovich.periodicals.dao.mysql.exception.MySqlDaoException;
import by.bsuir.gerasimovich.periodicals.entity.Orders;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public class MySqlOrdersDao extends AbstractJDBCDao<Orders> {

    private static MySqlOrdersDao instance;
    private final String SELECT = "SELECT orderId, dateOfOrder, clientId, status, statusRemove FROM orders WHERE statusRemove= false;";
    private final String SELECT_BY_PK = "SELECT orderId, dateOfOrder, clientId, status, statusRemove FROM orders WHERE statusRemove= false AND orderId=?;";
    private final String SELECT_BY_USER = "SELECT orderId, dateOfOrder, clientId, status, statusRemove FROM orders WHERE statusRemove= false AND clientId = ?;";
    private final String INSERT = "INSERT INTO orders ( dateOfOrder, clientId, status, statusRemove) \n"
            + "VALUES ( ?, ?, ?, ?);";
    private final String UPDATE = "UPDATE orders SET dateOfOrder = ?, clientId = ?, status = ?, statusRemove =?  WHERE orderId= ?;";
    private final String UPDATE_FOR_DELETE = "UPDATE orders SET statusRemove = 1  WHERE orderId= ?;";

    private final String DELETE = "DELETE FROM orders WHERE orderId = ?;";

    private MySqlOrdersDao() throws DAOException {
        super();

    }

    public static MySqlOrdersDao getInstance() throws DAOException {
        MySqlOrdersDao localInstance = instance;
        if (localInstance == null) {
            synchronized (MySqlOrdersDao.class) {
                localInstance=instance;
                if (localInstance == null) {
                    instance = localInstance= new MySqlOrdersDao();
                }
            }
        }
        return localInstance;
    }

    @Override
    public String getSelectQuery() {
        return SELECT;
    }

    @Override
    public String getSelectByPkQuery() {
        return SELECT_BY_PK;
    }

    @Override
    public String getCreateQuery() {
        return INSERT;
    }

    @Override
    public String getUpdateQuery() {

        return UPDATE;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE;
    }

    @Override
    protected List<Orders> parseResultSet(ResultSet rs) throws DAOException {
        LinkedList<Orders> result = new LinkedList<Orders>();
        try {
            while (rs.next()) {

                Orders order = new Orders();
                order.setId(rs.getInt("orderId"));
                order.setDateOfOrder(rs.getDate("dateOfOrder"));
                order.setClientId(rs.getInt("clientId"));
                order.setStatus(rs.getInt("status"));
                order.setStatusRemove(rs.getBoolean("statusRemove"));
                result.add(order);
            }
        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Orders object) throws DAOException {
        try {
            statement.setDate(1, object.getDateOfOrder());
            statement.setInt(2, object.getClientId());
            statement.setInt(3, object.getStatus());
            statement.setBoolean(4, object.getStatusRemove());

        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Orders object) throws DAOException {
        try {
            statement.setDate(1, object.getDateOfOrder());
            statement.setInt(2, object.getClientId());
            statement.setInt(3, object.getStatus());
            statement.setBoolean(4, object.getStatusRemove());
            statement.setInt(5, object.getId());
        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
    }

    public List<Orders> getAllByUser(int clientId) throws DAOException {
        List<Orders> list;
        ResultSet rs = null;

        try (PreparedStatement statement = getConnection().prepareStatement(SELECT_BY_USER)) {
            statement.setInt(1, clientId);
            rs = statement.executeQuery();
            list = parseResultSet(rs);
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                putConnection();
            } catch (SQLException e) {
                throw new DAOException(e);
            }

        }
        return list;
    }

    public void updateForDelete(int orderId) throws DAOException {
        try (PreparedStatement statement = getConnection().prepareStatement(UPDATE_FOR_DELETE)) {
            statement.setInt(1, orderId);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DAOException("On update modify more then 1 record: " + count);
            }
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
                putConnection();

        }
    }
}