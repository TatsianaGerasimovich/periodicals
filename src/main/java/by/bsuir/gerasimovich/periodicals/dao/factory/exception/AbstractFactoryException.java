package by.bsuir.gerasimovich.periodicals.dao.factory.exception;

import by.bsuir.gerasimovich.periodicals.exeption.PeriodicalException;

/**
 * Exception class for an abstract factory
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public class AbstractFactoryException extends PeriodicalException {
    /**
     * default constructor
     */
    public AbstractFactoryException() {
    }

    /**
     * constructor parameters
     * @param message
     */
    public AbstractFactoryException(String message) {
        super(message);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     */
    public AbstractFactoryException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor parameters
     * @param cause
     */
    public AbstractFactoryException(Throwable cause) {
        super(cause);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public AbstractFactoryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
