package by.bsuir.gerasimovich.periodicals.dao.pool;

import by.bsuir.gerasimovich.periodicals.dao.pool.exception.ConnectionPoolException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * The class implements
 * the connection pool to the database sql.
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public class ConnectionPool {

    /**
     * Configuration constants for the need to create a pool
     */
    private static final Map<String, String> configInformation = ConfigurationManager.getConfigInformation();
    private static final String DRIVER_NAME = configInformation.get(ConfigurationManager.DATABASE_DRIVER_NAME);
    private static final String URL = configInformation.get(ConfigurationManager.DATABASE_URL);
    private static final String LOGIN = configInformation.get(ConfigurationManager.DATABASE_LOGIN);
    private static final String PASSWORD = configInformation.get(ConfigurationManager.DATABASE_PASSWORD);
    private static final int MAX_CONNECTION_COUNT = Integer.parseInt(configInformation.get(ConfigurationManager.DATABASE_MAX_CONNECTION_VALUE));
    private BlockingQueue<Connection> pool = new ArrayBlockingQueue<Connection>(MAX_CONNECTION_COUNT, true);
    private static final int MIN_CONNECTION_COUNT = Integer.parseInt(configInformation.get(ConfigurationManager.DATABASE_MIN_CONNECTION_VALUE));
    /**
     * Variable leading accounting of current connections
     */
    private volatile int currentConnectionNumber = MIN_CONNECTION_COUNT;
    public static volatile ConnectionPool instance;
    public static final Logger LOG = Logger.getLogger(ConnectionPool.class);

    public static ConnectionPool getInstance() throws ConnectionPoolException {
        ConnectionPool localInstance = instance;
        if (localInstance == null) {
            synchronized (ConnectionPool.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ConnectionPool();
                }
            }
        }
        return localInstance;
    }

    /**
     * The constructor creates an instance of the pool.
     * Initializes a constant number of connections - 5.
     */
    private ConnectionPool() {

    }

    public void initPoolData() throws ConnectionPoolException {
        try {
            Class.forName(DRIVER_NAME);

            for (int i = 0; i < MIN_CONNECTION_COUNT; i++) {

                pool.add(DriverManager.getConnection(URL, LOGIN, PASSWORD));
            }
        } catch (SQLException e) {
            throw new ConnectionPoolException("Error in completing the connection pool");
        } catch (ClassNotFoundException ex) {
            throw new ConnectionPoolException("can't find database driver class", ex);
        }
    }

    /**
     * The method provides the user with a copy of connection from pool
     *
     * @return Connection
     * @see java.sql.Connection
     */
    public Connection getConnection() throws ConnectionPoolException {
        Connection connection = null;
        try {
            if (pool.isEmpty() && currentConnectionNumber < MAX_CONNECTION_COUNT) {
                openAdditionalConnection();
            }
            connection = pool.take();
        } catch (InterruptedException exception) {
            throw new ConnectionPoolException("Thread executes a blocking call is interrupted. Error in taking connection");
        }
        return connection;
    }

    /**
     * The method is returned, the connection back to the pool
     * when you are finished work with him.
     *
     * @param connection
     */
    public void closeConnection(Connection connection) throws ConnectionPoolException {
        if (connection != null) {

            if (currentConnectionNumber > MIN_CONNECTION_COUNT) {
                currentConnectionNumber--;
            }

            try {
                pool.put(connection);
            } catch (InterruptedException exception) {
                throw new ConnectionPoolException("Thread executes a blocking call is interrupted. Error in putting connection");
            }

        }
    }

    /**
     * Method , if necessary, to provide additional
     * connection to the database
     */
    private void openAdditionalConnection() throws ConnectionPoolException {
        try {
            Class.forName(DRIVER_NAME);
        } catch (ClassNotFoundException exception) {
            throw new ConnectionPoolException("Driver is not found");
        }
        try {
            pool.add(DriverManager.getConnection(URL, LOGIN, PASSWORD));
            currentConnectionNumber++;
        } catch (SQLException exception) {
            throw new ConnectionPoolException("Error in opening the additional connection");
        }
    }

    /**
     * Method closes all connections
     */
    public void dispose(){
        try {
        Connection connection;
        while ((connection = pool.poll()) != null) {
            if (!connection.getAutoCommit()) {
                connection.commit();
            }
            connection.close();
        }
        } catch (SQLException ex) {
            LOG.error("error iin disposing connection pool", ex);
        }
    }

}