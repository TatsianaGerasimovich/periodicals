package by.bsuir.gerasimovich.periodicals.service;

import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;
import by.bsuir.gerasimovich.periodicals.dao.factory.AbstractDAOFactory;
import by.bsuir.gerasimovich.periodicals.dao.factory.exception.AbstractFactoryException;
import by.bsuir.gerasimovich.periodicals.dao.mysql.*;
import by.bsuir.gerasimovich.periodicals.entity.*;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 18.05.2015.
 */
public class GuestService {
    private static final Logger LOG = Logger.getLogger(GuestService.class);
    private static volatile GuestService instance;

    private GuestService() {
    }

    public static GuestService getInstance() {
        GuestService localInstance = instance;
        if (localInstance == null) {
            synchronized (GuestService.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new GuestService();
                }
            }
        }
        return localInstance;
    }

    public ServiceResponse periodicalList() throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlCategoryDao mySqlCategoryDao = (MySqlCategoryDao) factory.getDao(Category.class);
            MySqlPeriodicalsDao mySqlPeriodicalsDao = (MySqlPeriodicalsDao) factory.getDao(Periodicals.class);
            List<Category> categoryList = mySqlCategoryDao.getAll();
            List<Periodicals> periodicalsList = mySqlPeriodicalsDao.getAll();

            ServiceResponse response = new ServiceResponse();
            response.put(ServiceResponse.PERIODICALS, periodicalsList);
            response.put(ServiceResponse.CATEGORIES, categoryList);
            return response;

        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException("unable to get periodicalList", e);
        }
    }

    public ServiceResponse periodicalListByCategory(int categoryId) throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlCategoryDao mySqlCategoryDao = (MySqlCategoryDao) factory.getDao(Category.class);
            MySqlPeriodicalsDao mySqlPeriodicalsDao = (MySqlPeriodicalsDao) factory.getDao(Periodicals.class);
            List<Category> categoryList = mySqlCategoryDao.getAll();
            List<Periodicals> periodicalsList;
            if(categoryId!=0) {
                 periodicalsList = mySqlPeriodicalsDao.getAllByCategory(categoryId);
            }
            else{
                periodicalsList = mySqlPeriodicalsDao.getAll();
            }
            ServiceResponse response = new ServiceResponse();
            response.put(ServiceResponse.PERIODICALS, periodicalsList);
            response.put(ServiceResponse.CATEGORIES, categoryList);
            return response;
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException("unable to get periodicalListByCategory", e);
        }
    }

    public ServiceResponse showPeriodical(int periodicalId) throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlPeriodicalsDao mySqlPeriodicalsDao = (MySqlPeriodicalsDao) factory.getDao(Periodicals.class);
            MySqlReviewsDao mySqlReviewsDao = (MySqlReviewsDao) factory.getDao(Reviews.class);
            MySqlPicturesDao mySqlPicturesDao = (MySqlPicturesDao) factory.getDao(Pictures.class);
            MySqlUsersDao mySqlUsersDao = (MySqlUsersDao) factory.getDao(Users.class);
            List<Reviews> reviewsList = mySqlReviewsDao.getAllByPeriodical(periodicalId);
            List<Pictures> picturesList = mySqlPicturesDao.getAllByPeriodical(periodicalId);
            String mainPicture = null;
            Iterator itr = picturesList.iterator();
            List<Users> users = new ArrayList<>();
            List<Pictures> firstPhoto = new ArrayList<>();
            int half = picturesList.size() / 2;
            boolean flag=false;
            for (int i = 0; i < half; ) {
                Pictures element = (Pictures) itr.next();
                if (element.getIsMain()) {
                    mainPicture = element.getPicturePath();
                    flag=true;
                } else {
                    firstPhoto.add(element);
                    i++;
                }

            }
            int half2 = picturesList.size() - half;
            if(flag)
                half2-=1;
            List<Pictures> lastPhoto = new ArrayList<>();
            for (int i = 0; i < half2; i++) {
                Pictures element = (Pictures) itr.next();
                if (element.getIsMain()) {
                    mainPicture = element.getPicturePath();

                } else {
                    lastPhoto.add(element);

                }
            }

            for (Reviews obj : reviewsList) {
                users.add(mySqlUsersDao.getByPK(obj.getUserId()));
            }
            ServiceResponse response = new ServiceResponse();
            response.put(ServiceResponse.PERIODICAL, mySqlPeriodicalsDao.getByPK(periodicalId));
            response.put(ServiceResponse.REVIEWS, reviewsList);
            response.put(ServiceResponse.FIRST_PHOTOS, firstPhoto);
            response.put(ServiceResponse.LAST_PHOTOS, lastPhoto);
            response.put(ServiceResponse.MAIN_PICTURE, mainPicture);
            response.put(ServiceResponse.USERS, users);
            return response;

        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public ServiceResponse signInPage() throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlPeriodicalsDao mySqlPeriodicalsDao = (MySqlPeriodicalsDao) factory.getDao(Periodicals.class);

            List<Periodicals> periodicalsList = mySqlPeriodicalsDao.getAll();
            Iterator itr = periodicalsList.iterator();
            List<Periodicals> firstPhoto = new ArrayList<>();
            int half = periodicalsList.size() / 2;
            for (int i = 0; i < half; i++) {
                Periodicals element = (Periodicals) itr.next();
                firstPhoto.add(element);
            }
            int half2 = periodicalsList.size() - half;
            List<Periodicals> lastPhoto = new ArrayList<>();
            for (int i = 0; i < half2; i++) {
                Periodicals element = (Periodicals) itr.next();
                lastPhoto.add(element);
            }

            ServiceResponse response = new ServiceResponse();
            response.put(ServiceResponse.FIRST_PHOTOS, firstPhoto);
            response.put(ServiceResponse.LAST_PHOTOS, lastPhoto);
            return response;

        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public Users signIn(String login, String password) throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlUsersDao mySqlUsersDao = (MySqlUsersDao) factory.getDao(Users.class);

            List<Users> users = mySqlUsersDao.getAllByLogin(login);
            if (users.size() == 0) {
                return null;
            } else {
                Users user = users.get(0);
                if (user.getUserPassword().equals(password)) {
                    if (user.getUserRole().equals(Users.ADMIN)) {
                        user.setUserRole(Users.ADMIN);
                    } else {
                        user.setUserRole(Users.USER);
                        user.setCart(new PeriodicalCart());
                    }
                    return user;
                } else {
                    return null;
                }
            }
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public Users signUp(String login,String password, String name,String email,
                        String country, String city, String address, double postcode, String photo) throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlUsersDao mySqlUsersDao = (MySqlUsersDao) factory.getDao(Users.class);
            List<Users> users = mySqlUsersDao.getAll();
            List<String> logins = new ArrayList<>();
            for (Users item : users) {
                logins.add(item.getLogin());
            }
            if (logins.contains(login)) {
                return null;
            }
            Users user = new Users( 0,email, address, city,
            postcode,country,photo ,
                    login,password,Users.USER, name );
            int id=mySqlUsersDao.create(user);
            user.setCart(new PeriodicalCart());
            user.setUserId(id);
            return user;
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

}
