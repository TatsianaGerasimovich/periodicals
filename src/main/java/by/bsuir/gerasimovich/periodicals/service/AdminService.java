package by.bsuir.gerasimovich.periodicals.service;

import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;
import by.bsuir.gerasimovich.periodicals.dao.factory.AbstractDAOFactory;
import by.bsuir.gerasimovich.periodicals.dao.factory.exception.AbstractFactoryException;
import by.bsuir.gerasimovich.periodicals.dao.mysql.*;
import by.bsuir.gerasimovich.periodicals.entity.*;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 18.05.2015.
 */
public class AdminService {
    private static final Logger LOG = Logger.getLogger(AdminService.class);
    private static volatile AdminService instance;

    private AdminService() {
    }

    public static AdminService getInstance() {
        AdminService localInstance = instance;
        if (localInstance == null) {
            synchronized (AdminService.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new AdminService();
                }
            }
        }
        return localInstance;
    }
    public ServiceResponse getUsers() throws ServiceException {
        try{
        MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
        MySqlUsersDao mySqlUsersDao = (MySqlUsersDao) factory.getDao(Users.class);
        List<Users> users = mySqlUsersDao.getAll();
            ServiceResponse response = new ServiceResponse();
            response.put(ServiceResponse.USERS, users);
            return response;
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
    public void removeUser(Users user) throws ServiceException {
        try {

            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlUsersDao mySqlUsersDao = (MySqlUsersDao) factory.getDao(Users.class);
            MySqlOrdersDao mySqlOrdersDao = (MySqlOrdersDao) factory.getDao(Orders.class);
            MySqlReviewsDao mySqlReviewsDao = (MySqlReviewsDao) factory.getDao(Reviews.class);

            mySqlUsersDao.updateForDelete(user.getUserId());
            List<Orders> orders = mySqlOrdersDao.getAllByUser(user.getUserId());
            for (Orders item : orders) {
                mySqlOrdersDao.updateForDelete(item.getOrderId());
            }
            List<Reviews> reviewsList = mySqlReviewsDao.getAllByUser(user.getUserId());
            for (Reviews item : reviewsList) {
                mySqlReviewsDao.updateForDelete(item.getReviewId());
            }
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public ServiceResponse getAllOrders() throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlOrdersDao mySqlOrdersDao = (MySqlOrdersDao) factory.getDao(Orders.class);
            MySqlUsersDao mySqlUsersDao = (MySqlUsersDao) factory.getDao(Users.class);
            MySqlOrderPeriodicalsDao mySqlOrderPeriodicalsDao = (MySqlOrderPeriodicalsDao) factory.getDao(OrderPeriodicals.class);
            MySqlPeriodicalsDao mySqlPeriodicalsDao = (MySqlPeriodicalsDao) factory.getDao(Periodicals.class);
            List<Orders> orders = mySqlOrdersDao.getAll();
            for (Orders item : orders) {
                List<OrderPeriodicals> orderPeriodicalses = mySqlOrderPeriodicalsDao.getAllByOrder(item.getOrderId());
                List<PeriodicalInf> periodicalInfList = new ArrayList<>();
                for (OrderPeriodicals obj : orderPeriodicalses) {
                    periodicalInfList.add(new PeriodicalInf(mySqlPeriodicalsDao.getByPK(obj.getPeriodicalId()), obj.getNumber()));
                }
                item.setPeriodicals(periodicalInfList);
            }
            List<Users> users = new ArrayList<>();
            for (Orders obj : orders) {
                users.add(mySqlUsersDao.getByPK(obj.getClientId()));
            }
            ServiceResponse response = new ServiceResponse();
            response.put(ServiceResponse.ORDERS, orders);
            response.put(ServiceResponse.USERS, users);

            return response;

        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void makeUserAdmin(Users user) throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlUsersDao mySqlUsersDao = (MySqlUsersDao) factory.getDao(Users.class);
            List<Users> users = mySqlUsersDao.getAllByLogin(user.getLogin());
            Users newUser= users.get(0);
            newUser.setUserRole(Users.ADMIN);
            mySqlUsersDao.update(newUser);
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
    public void paidOrder(Orders order)
            throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlOrdersDao mySqlOrdersDao = (MySqlOrdersDao) factory.getDao(Orders.class);
            Orders newOrder = mySqlOrdersDao.getByPK(order.getOrderId());
            newOrder.setStatus(1);
            mySqlOrdersDao.update(newOrder);
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void removeOrder(Orders order) throws ServiceException {
        try {

            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlOrdersDao mySqlOrdersDao = (MySqlOrdersDao) factory.getDao(Orders.class);
            mySqlOrdersDao.updateForDelete(order.getId() );
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public ServiceResponse periodicalList() throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlPeriodicalsDao mySqlPeriodicalsDao = (MySqlPeriodicalsDao) factory.getDao(Periodicals.class);
            List<Periodicals> periodicalsList = mySqlPeriodicalsDao.getAll();

            ServiceResponse response = new ServiceResponse();
            response.put(ServiceResponse.PERIODICALS, periodicalsList);
            return response;

        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException("unable to get periodicalList", e);
        }
    }
    public void removePeriodical(Periodicals periodical) throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlPeriodicalsDao mySqlPeriodicalsDao = (MySqlPeriodicalsDao) factory.getDao(Periodicals.class);
            mySqlPeriodicalsDao.updateForDelete(periodical.getId());
            MySqlOrdersDao mySqlOrdersDao = (MySqlOrdersDao) factory.getDao(Orders.class);
            MySqlOrderPeriodicalsDao mySqlOrderPeriodicalsDao =(MySqlOrderPeriodicalsDao) factory.getDao(OrderPeriodicals.class);
            List<OrderPeriodicals> orderPeriodicalsList =mySqlOrderPeriodicalsDao.getAllByPeriodical(periodical.getId());
            for (OrderPeriodicals item : orderPeriodicalsList) {
                mySqlOrdersDao.updateForDelete(item.getOrderId());
            }
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
    public ServiceResponse categoryList() throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlCategoryDao mySqlCategoryDao = (MySqlCategoryDao) factory.getDao(Category.class);
            List<Category> categoryList = mySqlCategoryDao.getAll();

            ServiceResponse response = new ServiceResponse();
            response.put(ServiceResponse.CATEGORIES, categoryList);
            return response;

        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException("unable to get periodicalList", e);
        }
    }

    public Periodicals createPeriodical(String name,String description,
                                  double price, int categoryId, String photo) throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlPeriodicalsDao mySqlPeriodicalsDao = (MySqlPeriodicalsDao) factory.getDao(Periodicals.class);
            Periodicals newPeriodical = new Periodicals(0, name,
                    description, price,photo,categoryId);
            int id=mySqlPeriodicalsDao.create(newPeriodical);
            newPeriodical.setId(id);
            return newPeriodical;
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

}
