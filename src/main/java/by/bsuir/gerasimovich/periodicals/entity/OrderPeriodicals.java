package by.bsuir.gerasimovich.periodicals.entity;

/**
 * Class-Entity store the information about <entity>OrderPeriodicals</entity>
 * Part of the class hierarchy of entities
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.entity.GeneralEntity
 */
public class OrderPeriodicals implements GeneralEntity{
    private int orderId;
    private int periodicalId;
    private int number;

    public Integer getId() {
        return null;
    }

    public void setId(int orderId) {

    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getPeriodicalId() {
        return periodicalId;
    }

    public void setPeriodicalId(int periodicalId) {
        this.periodicalId = periodicalId;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof OrderPeriodicals)) return false;

        OrderPeriodicals that = (OrderPeriodicals) o;

        if (number != that.number) return false;
        if (orderId != that.orderId) return false;
        if (periodicalId != that.periodicalId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = orderId;
        result = 31 * result + periodicalId;
        result = 31 * result + number;
        return result;
    }

    @Override
    public String toString() {
        return "OrderPeriodicals{" +
                "orderId=" + orderId +
                ", periodicalId=" + periodicalId +
                ", number=" + number +
                '}';
    }
}
