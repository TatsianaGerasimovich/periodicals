package by.bsuir.gerasimovich.periodicals.entity;

/**
 * Class is the ancestor of the class hierarchy of
 * entities and describes the behavior characteristic of all entities.
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public interface GeneralEntity {
    public Integer getId();
    public void setId(int id);
}
