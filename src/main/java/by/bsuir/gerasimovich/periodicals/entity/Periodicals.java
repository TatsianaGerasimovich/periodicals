package by.bsuir.gerasimovich.periodicals.entity;

/**
 * Class-Entity store the information about <entity>Periodicals</entity>
 * Part of the class hierarchy of entities
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.entity.GeneralEntity
 */
public class Periodicals implements GeneralEntity{
    private int periodicalId;
    private String periodicalName;
    private String periodicalDescription;
    private Double price;
    private String coverOfPeriodical;
    private int categoryId;
    private Boolean statusRemove;

    public Periodicals(int periodicalId, String periodicalName,
                       String periodicalDescription, Double price,
                       String coverOfPeriodical,int categoryId) {
        this.periodicalId = periodicalId;
        this.periodicalName = periodicalName;
        this.periodicalDescription = periodicalDescription;
        this.price=price;
        this.coverOfPeriodical = coverOfPeriodical;
        this.categoryId = categoryId;
        this.statusRemove = false;
    }

    public Periodicals() {
        this.statusRemove = false;
    }

    public Integer getId() {
        return periodicalId;
    }

    public void setId(int periodicalId) {
        this.periodicalId = periodicalId;
    }

    public int getPeriodicalId() {
        return periodicalId;
    }

    public void setPeriodicalId(int periodicalId) {
        this.periodicalId = periodicalId;
    }

    public String getPeriodicalName() {
        return periodicalName;
    }

    public void setPeriodicalName(String periodicalName) {
        this.periodicalName = periodicalName;
    }

    public String getPeriodicalDescription() {
        return periodicalDescription;
    }

    public void setPeriodicalDescription(String periodicalDescription) {
        this.periodicalDescription = periodicalDescription;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCoverOfPeriodical() {
        return coverOfPeriodical;
    }

    public void setCoverOfPeriodical(String coverOfPeriodical) {
        this.coverOfPeriodical = coverOfPeriodical;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public Boolean getStatusRemove() {
        return statusRemove;
    }

    public void setStatusRemove(Boolean statusRemove) {
        this.statusRemove = statusRemove;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof Periodicals)) return false;

        Periodicals that = (Periodicals) o;

        if (categoryId != that.categoryId) return false;
        if (periodicalId != that.periodicalId) return false;
        if (coverOfPeriodical != null ? !coverOfPeriodical.equals(that.coverOfPeriodical) : that.coverOfPeriodical != null)
            return false;
        if (periodicalDescription != null ? !periodicalDescription.equals(that.periodicalDescription) : that.periodicalDescription != null)
            return false;
        if (periodicalName != null ? !periodicalName.equals(that.periodicalName) : that.periodicalName != null)
            return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (statusRemove != null ? !statusRemove.equals(that.statusRemove) : that.statusRemove != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = periodicalId;
        result = 31 * result + (periodicalName != null ? periodicalName.hashCode() : 0);
        result = 31 * result + (periodicalDescription != null ? periodicalDescription.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (coverOfPeriodical != null ? coverOfPeriodical.hashCode() : 0);
        result = 31 * result + categoryId;
        result = 31 * result + (statusRemove != null ? statusRemove.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Periodicals{" +
                "periodicalId=" + periodicalId +
                ", periodicalName='" + periodicalName + '\'' +
                ", periodicalDescription='" + periodicalDescription + '\'' +
                ", price=" + price +
                ", coverOfPeriodical='" + coverOfPeriodical + '\'' +
                ", categoryId=" + categoryId +
                ", statusRemove=" + statusRemove +
                '}';
    }
}
