<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 25.05.2015
  Time: 22:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="locale"
       value="${locale}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="messages"/>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <title>Details</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="jsp/css/bootstrap.min.css">
    <link rel="stylesheet" href="jsp/css/normalize.min.css">
    <link rel="stylesheet" href="jsp/css/font-awesome.min.css">
    <link rel="stylesheet" href="jsp/css/animate.css">
    <link rel="stylesheet" href="jsp/css/templatemo_misc.css">
    <link rel="stylesheet" href="jsp/css/templatemo_style.css">

    <script src="jsp/js/vendor/jquery-1.10.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="jsp/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
    <script src="jsp/js/jquery.easing-1.3.js"></script>
    <script src="jsp/js/vendor/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript">
        function addToCart(id) {
            var periodical = {
                periodicalId: id
            }
            var data = {
                periodical: periodical,
                number: 1
            }
            // get inputs
            var request = new Object();
            request.data = JSON.stringify(data);
            request.command = 'add_to_cart';

            $.ajax({
                url: "Controller",
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(request),
                contentType: 'application/json',
                mimeType: 'application/json',

                success: function (data) {
                    if (data == -1) {
                        alert('You have already add this book to order. Go to order to change number');
                        //  $('#errorMessage').text('You have already add this book to order. Go to order to change number');
                    }
                    else {
                        $('#periodicalsInOrder').text(data);
                    }
                },
                error: function (data, status, er) {
                    alert("error: " + data + " status: " + status + " er:" + er);
                }
            });
        }

    </script>

</head>
<body>
<%@include file="header.jsp" %>

<div id="products" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="section-title"><fmt:message key="periodical.title"/></h1>
                <c:if test="${user.userRole eq 'user'}">
                    <a class="comment-title" href="#" onclick="addToCart(${periodicalItem.periodicalId})"><fmt:message key="periodical.add_to_cart"/></a>
                </c:if>
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-md-2 col-sm-3">
                <c:forEach var="photo_f" items="${first_photos}">
                    <div class="item-small">
                        <img src="${photo_f.picturePath}" alt="Product 1">
                    </div>
                    <!-- /.item-small -->
                    <br/>
                </c:forEach>
            </div>
            <!-- /.col-md-2 -->

            <div class="col-md-8 col-sm-6">
                <div class="item-large">
                    <c:set value="${main_picture}" var="picture"></c:set>
                    <img src="${picture}" alt="Product 2">
                    <c:set value="${periodical}" var="periodicalItem"></c:set>
                    <div class="item-large-content">
                        <div class="item-header">
                            <h2 class="text-center">${periodicalItem.periodicalName}</h2>

                            <div class="clearfix"></div>
                        </div>
                        <!-- /.item-header -->
                        <p>${periodicalItem.periodicalDescription}</p>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h1 class="section-title"><fmt:message key="comments.title"/></h1>
                            </div>
                            <!-- /.col-md-12 -->
                        </div>
                        <!-- /.row -->
                        <c:set value="${reviews}" var="review"></c:set>
                        <c:forEach var="userItem" items="${users}" varStatus="status">
                            <div class="row">
                                <fieldset class="col-md-2">
                                    <img src="${userItem.userPhoto}" alt="Product 1">
                                </fieldset>
                                <fieldset class="col-md-10">
                                    <h4 class="comment-title">${userItem.userName}</h4>
                                    <h5>${review[status.index].reviewDescription}</h5>
                                </fieldset>
                            </div>
                            </br>
                        </c:forEach>

                        <c:if test="${user.userRole eq 'user'}">
                            <form action="Controller" id="Main" method="post">
                                <input type="hidden" name="command" value="create_comment">
                                <input type="hidden" name="periodicalId" value="${periodicalItem.periodicalId}">
                            <div class="row contact-form">
                                <fieldset class="col-md-12">
                                    <textarea name="comment" id="comments" placeholder="Message" required></textarea>
                                </fieldset>
                                <fieldset class="col-md-12">
                                    <input type="submit" name="send" value="<fmt:message key="comments.send"/>" id="submit" class="button">
                                </fieldset>
                            </div>
                            </form>
                            <!-- /.contact-form -->
                        </c:if>

                    </div>
                    <!-- /.item-large-content -->
                </div>
                <!-- /.item-large -->
            </div>
            <!-- /.col-md-8 -->
            <div class="col-md-2 col-sm-3">
                <c:forEach var="photo" items="${last_photos}">
                    <div class="item-small">
                        <img src="${photo.picturePath}" alt="Product 1">
                    </div>
                    <!-- /.item-small -->
                    <br/>
                </c:forEach>
            </div>
            <!-- /.col-md-2 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /#product-promotion -->


<div class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                    <span>
                    	Copyright &copy; 2015 Gerasimovich Tatiana
                    </span>

            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6">
                <ul class="social">
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-instagram"></a></li>
                    <li><a href="#" class="fa fa-linkedin"></a></li>
                    <li><a href="#" class="fa fa-rss"></a></li>
                </ul>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /.site-footer -->


<!-- Scroll to Top -->
<div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>


<script src="jsp/js/bootstrap.js"></script>
<script src="jsp/js/plugins.js"></script>
<script src="jsp/js/main.js"></script>
<script type="jsp/text/javascript">

    $(document).ready(function() {
        $(function() {
            $(window).scroll(function() {
                if($(this).scrollTop() != 0) {
                    $('#toTop').fadeIn();
                } else {
                    $('#toTop').fadeOut();
                }
            });

            $('#toTop').click(function() {
                $('body,html').animate({scrollTop:0},800);
            });
        });
    });

</script>
</body>
</html>
